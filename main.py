import sys
from PyQt5 import QtGui, QtCore
from PyQt5.QtCore import Qt

from PyQt5.QtWidgets import QWidget, QApplication, QMainWindow
from PyQt5 import uic

class MainWindow(QMainWindow):
    X, Y = int, int
    mouse_moved = QtCore.pyqtSignal(X, Y)

    def __init__(self):
        super().__init__()
        self.initUI()

    def initUI(self):
        uic.loadUi('./look_here.ui', self)
        self.setMouseTracking(True)

        self.image.set_image('./Sheer_Heart_Attack_color.png')

        self.coords.setAttribute(Qt.WA_TransparentForMouseEvents)
        self.image.setAttribute(Qt.WA_TransparentForMouseEvents)

        self.mouse_moved.connect(self.coords.mouse_move_event)
        self.coords.look_over_here.connect(self.image.image_display)

    def mouseMoveEvent(self, a0: QtGui.QMouseEvent) -> None:
        self.mouse_moved.emit(a0.x(), a0.y())


if __name__ == '__main__':
    app = QApplication(sys.argv)
    ex = MainWindow()
    ex.show()
    sys.exit(app.exec())
