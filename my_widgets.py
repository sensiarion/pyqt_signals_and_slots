from PyQt5 import QtCore
from PyQt5.QtCore import QPoint, QTimer
from PyQt5.QtGui import QPixmap
from PyQt5.QtWidgets import QWidget, QLabel


class CoordDisplayWidget(QWidget):
    look_over_here = QtCore.pyqtSignal()

    def __init__(self, parent: QWidget):
        parent.setMouseTracking(True)

        super().__init__(parent)
        self.initUI()

    def initUI(self):
        self.coords = QLabel(self)
        self.coords.setText("Координаты: None, None")
        self.coords.move(30, 30)
        self.setMouseTracking(True)

    @QtCore.pyqtSlot(int, int)
    def mouse_move_event(self, x, y):
        self.coords.setText(f"Координаты: {x}, {y}")
        if not self.rect().contains(QPoint(x, y)):
            self.look_over_here.emit()


class FlushingImage(QLabel):
    def __init__(self, parent, img_path: str = None, flush_interval: int = 1000):
        super().__init__(parent)
        self.flush_interval = flush_interval
        self.image = QPixmap(img_path)
        self.timer = QTimer()

        self.timer.timeout.connect(self.image_flush)

    def set_image(self, img_path: str):
        self.image = QPixmap(img_path)

    def image_flush(self):
        self.clear()

    @QtCore.pyqtSlot()
    def image_display(self):
        if not self.image:
            raise ValueError("Image should be set before use")
        self.setPixmap(self.image)
        self.adjustSize()

        if not self.timer.isActive():
            self.timer.start(self.flush_interval)
